<?php include( 'header.php' ); ?>

<!-- START:: LOOP DE SEÇÃO -->



<!-- NOTA
    Tem uma classe desabilitado que vai ser aplicado quando o curso não estiver ativo.
-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="titulo-secao">Essa Semana</h2>
            </div>

            <div class="col-sm-3">
                <div class="box-loop-cursos desabilitado">
                    <a href="aulas.php">
                        <img src="./assets/img/teste-logo-lev-conceito.jpg" class="img-fluid" >
                        <p>Apresente seus projetos com imagens conceituais e diagramas</p>
                        <span class="material-icons-outlined">lock</span>
                    </a>

                </div><!-- end:: .box-loop-cursos -->
            </div><!-- end:: .col-sm-3 -->

            <div class="col-sm-3">
                <div class="box-loop-cursos">

                    <a href="aulas.php">
                        <img src="./assets/img/teste-logo-lev-conceito.jpg" class="img-fluid" >
                        <p>Apresente seus projetos com imagens conceituais e diagramas</p>
                        <span class="material-icons-outlined">done</span>
                    </a>

                </div><!-- end:: .box-loop-cursos -->
            </div><!-- end:: .col-sm-3 -->


        </div>
    </div>
</section>

<!-- END:: LOOP DE SEÇÃO -->

<?php include( 'footer.php' );