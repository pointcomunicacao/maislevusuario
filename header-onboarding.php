<!doctype html>
<html lang="pt-br">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Título -->
    <title>+Lev EAD</title>
    <meta name="author" content="Wesley Oliveira - Point Comunicação® - https://pointcomunicacao.ppg.br">

    <?php include( './includes/metatags.php' );?>
    <?php include( './includes/head.php' );?>

</head>
<body class="d-flex flex-column">

<!-- start:: #page-content -->
<div id="page-content">

<header>
    <div class="container">
        <div class="row">

            <!-- start:: logotipo -->
            <div class="col-md-4 offset-md-4 col-sm-12 d-flex justify-content-center">
                <a href="index.php" class="logotipo">
                    <img src="./assets/img/logotipo.png" alt="" class="img-fluid">
                </a>
            </div>
            <!-- end::logotipo -->

            <!-- start:: menu usuário -->
            <div class="col-md-4 col-sm-12 d-flex justify-content-end align-items-center">
                <?php include( './includes/menu-aluno.php' ); ?>
            </div>
            <!-- menu usuário -->

        </div><!-- end:: .row -->

    </div><!-- end:: container -->

</header>