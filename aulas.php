<?php include( 'header.php' ); ?>

<!-- NOTA
    Tem uma classe concluído que vai ser inserida em dois icones após conclusão..
-->

<div class="container">
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <form action="" class="pointcom-form">
                <div class="input-group">

                    <input type="search" class="form-control" placeholder="Pesquisar aula">

                    <button class="btn pointcom-btn" type="button">
                        <span class="material-icons-outlined">search</span>
                    </button>

                </div>
            </form>

            <div class="d-flex justify-content-center align-items-center">
                <a href="http://" class="btn btn-padrao">
                    Continuar de onde parei <span class="material-icons-outlined">arrow_right_alt</span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <!-- loop:: start:: card -->
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Comece Aqui: Uma Introdução para o Curso</h3>
                    <h6 class="card-subtitle text-muted">Essa é a introdução ou pré alinhamento do curso.</h6>
                    <hr>
                </div>

                <div class="table-responsive card-body">
                    <table class="table table-hover">
                        <tbody>
                            <!-- LOOP:: START:: ROW TABLE -->
                            <tr>
                                <td width="80">
                                    <a href="aula-sendo-apresentada.php">
                                        <span class="material-icons-outlined">play_circle_filled</span>
                                    </a>
                                </td>

                                <td class="text-muted">
                                    Essa é aula que vai fazer a introdução
                                </td>

                                <td width="100" class="text-muted">10’30”</td>

                                <td width="100">
                                    <span class="material-icons-outlined">check_circle_outline</span>
                                </td>
                            </tr>
                            <!-- LOOP:: END:: ROW TABLE -->

                            <!-- LOOP:: START:: ROW TABLE -->
                            <tr>
                                <td width="80">
                                    <a href="aula-sendo-apresentada.php">
                                        <span class="material-icons-outlined concluido">play_circle_filled</span>
                                    </a>
                                </td>

                                <td class="text-muted">
                                    Essa é outra aula
                                </td>

                                <td width="100" class="text-muted">13’34”</td>

                                <td width="100">
                                    <span class="material-icons concluido">check_circle</span>
                                </td>
                            </tr>
                            <!-- LOOP:: END:: ROW TABLE -->
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- loop:: end:: card -->

            <!-- loop:: start:: card -->
            <div class="card">
                <div class="media">
                    <span class="pointcom-number text-muted">
                        1
                    </span>
                    <div class="media-body card-body">
                        <h3 class="card-title">Primeiro Módulo do Curso</h3>
                        <h6 class="card-subtitle text-muted">Essa é a introdução ou pré alinhamento do curso.</h6>
                        <hr>
                    </div>
                </div>

                <div class="table-responsive card-body">
                    <table class="table table-hover">
                        <tbody>
                            <!-- LOOP:: START:: ROW TABLE -->
                            <tr>
                                <td width="80">
                                    <a href="">
                                        <span class="material-icons-outlined">play_circle_filled</span>
                                    </a>
                                </td>

                                <td class="text-muted">
                                    Essa é aula que vai fazer a introdução
                                </td>

                                <td width="100" class="text-muted">10’30”</td>

                                <td width="100">
                                    <span class="material-icons-outlined">check_circle_outline</span>
                                </td>
                            </tr>
                            <!-- LOOP:: END:: ROW TABLE -->

                            <!-- LOOP:: START:: ROW TABLE -->
                            <tr>
                                <td width="80">
                                    <a href="">
                                        <span class="material-icons-outlined concluido">play_circle_filled</span>
                                    </a>
                                </td>

                                <td class="text-muted">
                                    Essa é outra aula
                                </td>

                                <td width="100" class="text-muted">13’34”</td>

                                <td width="100">
                                    <span class="material-icons concluido">check_circle</span>
                                </td>
                            </tr>
                            <!-- LOOP:: END:: ROW TABLE -->
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- loop:: end:: card -->
        </div>
    </div>
</div>

<?php include( 'footer.php' );