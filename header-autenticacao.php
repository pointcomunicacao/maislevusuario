<!doctype html>
<html lang="pt-br">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Título -->
    <title>+Lev EAD</title>
    <meta name="author" content="Wesley Oliveira - Point Comunicação® - https://pointcomunicacao.ppg.br">
    
    <?php include( './includes/metatags.php' );?>
    <?php include( './includes/head.php' );?>

</head>
<body class="d-flex flex-column">

<!-- start:: #page-content -->
<div id="page-content">
