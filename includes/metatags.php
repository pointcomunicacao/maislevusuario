<meta property="og:locale" content="pt_BR">

<meta property="og:url" content="https://urldositeaqui.com.br">

<meta property="og:title" content="Título da página ou artigo">
<meta property="og:site_name" content="Nome do meu site">

<meta property="og:description" content="Descrição.">

<meta property="og:image" content="www.urldositeaqui.com.br/imagem.jpg">
<meta property="og:image:type" content="image/jpeg">
<meta property="og:image:width" content="800"> <!-- PIXELS -->
<meta property="og:image:height" content="600"> <!--PIXELS -->


<!--CASO SEJA UM SITE NORMAL -->

<meta property="og:type" content="website">

<!-- CASO SEJA UM ARTIGO  -->

<meta property="og:type" content="article">
<meta property="article:author" content="Autor do artigo">
<meta property="article:section" content="Seção do artigo">
<meta property="article:tag" content="Tags do artigo">
<meta property="article:published_time" content="date_time">

<!-- META TWITTER -->
<meta name="twitter:image:alt" content="Texto alt da imagem">
<meta name="twitter:card" content="summary_large_image">
