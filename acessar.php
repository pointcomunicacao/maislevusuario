<?php 

include( 'header-autenticacao.php' ); ?>

<section class="autenticacao">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6 box-left">
                <div class="pointcom-box-form">
                    <img src="./assets/img/logotipo-mais-lev.png" alt="" class="img-fluid logotipo">

                    <div class="box-info-form">
                    <h2>Entrar</h2>
                        <p>Se você já é aluno da Lev, é só preencher abaixo para acessar os cursos.</p>

                        <form action="" class="pointcom-form">
                            <input type="text" placeholder="Digite seu e-mail">
                            <input type="password" placeholder="Digite sua senha">
                            <a href="index.php" type="submit" class="btn-acessar">
                                Acessar
                            </a>
                        </form>

                        <div class="footer-autenticacao">
                            <a href="resetar-senha.php">Esqueci a senha</a>
                            <a href="">ainda não sou aluno</a>
                        </div>
                    </div>

                </div>
            
                    
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6 box-right">
               
            </div>
        </div>
    </div>
</section>

<?php include( 'footer-autenticacao.php' );