<?php 

include( 'header-autenticacao.php' ); ?>

<section class="autenticacao">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6 box-left">
                <div class="pointcom-box-form">
                    <img src="./assets/img/logotipo-mais-lev.png" alt="" class="img-fluid logotipo">
                    
                    <!--
                        A validação aqui vai sair assim Aldo

                        Se o usuário não existir entreo <p> e o <input> mostra o alerta
                        Se o usuário exitir ele oculta o forma e mostra a seguinte mensagem

                        Enviamos uma nova senha para o seu e-mail cadastrado.
                        Verifique seu e-mail

                     -->


                    <div class="box-info-form">
                        <h2>Recuperar a senha</h2>
                        <p>Insira o e-mail de sua conta no campo abaixo.</p>
                        
                        <!-- <p class="alerta">
                            Enviamos uma nova senha para o seu e-mail cadastrado.<br>
                            <b>Verifique seu e-mail</b>
                        </p> -->

                        <form action="" class="pointcom-form">
                            <input type="text" placeholder="Digite seu e-mail">
                            <a href="" type="submit" class="btn-acessar">
                                Recuperar senha
                            </a>
                        </form>

                        <div class="footer-autenticacao">
                            <a href="acessar.php">Me lembrei!</a>
                            <a href="">ainda não sou aluno</a>
                        </div>
                    </div>

                </div>
            
                    
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6 box-right">
               
            </div>
        </div>
    </div>
</section>

<?php include( 'footer-autenticacao.php' );