</div>
<!-- end:: #page-content -->

<!-- start:: footer -->
<footer id="sticky-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-flex justify-content-center align-items-center">
                <img src="./assets/img/logotipo-footer.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</footer>
<!-- end:: footer -->


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="./assets/bootstrap/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="https://player.vimeo.com/api/player.js"></script>
  </body>
</html>