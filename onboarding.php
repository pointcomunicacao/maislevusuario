<?php include('header-onboarding.php'); ?>

<section class="onboarding">
    <div class="container">

        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="card npd">
                    <div class="pointcom-video marb">
                        <iframe src="https://player.vimeo.com/video/49935360?color=e0a581&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="pointcom-onboarding-content">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-sm-4">
                <div class="card">
                    <div class="pointcom-number">
                        1
                    </div>

                    <h2>Queremos conhecer você</h2>

                    <p class="text-muted context">Você pode nos ajudar preenchendo esta pesquisa? é super rápido</p>

                    <a href="" class="btn btn-pointcom-preto">
                        Me apresentar <span class="material-icons-outlined">arrow_right_alt</span>
                    </a>

                    <p class="check"><span class="material-icons-outlined icon-right">check_circle_outline</span> você já fez isso?</p>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="pointcom-number">
                        2
                    </div>

                    <h2>Queremos conhecer você</h2>

                    <p class="text-muted context">Você pode nos ajudar preenchendo esta pesquisa? é super rápido</p>

                    <a href="" class="btn btn-pointcom-preto">
                        Me apresentar <span class="material-icons-outlined">arrow_right_alt</span>
                    </a>

                    <p class="check"><span class="material-icons-outlined icon-right">check_circle_outline</span> você já fez isso?</p>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="pointcom-number">
                        3
                    </div>

                    <h2>Queremos conhecer você</h2>

                    <p class="text-muted context">Você pode nos ajudar preenchendo esta pesquisa? é super rápido</p>

                    <a href="" class="btn btn-pointcom-preto">
                        Me apresentar <span class="material-icons-outlined">arrow_right_alt</span>
                    </a>

                    <p class="check"><span class="material-icons-outlined icon-right">check_circle_outline</span> você já fez isso?</p>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <a href="" class="pointcom-link">Eu farei isso mais tarde, me leve direto ao conteudo</a>
            </div>
        </div>
    </div>
</section>

<?php include('footer.php');
