<?php include( 'header.php' ); ?>

<section class="curso-on">
    <div class="container pointcom-box-resumido">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="pointcom-video">
                        <iframe src="https://player.vimeo.com/video/49935360?color=e0a581&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>

                    <div class="pointcom-conteudo-aulas">
                        <div class="row">

                            <div class="col-sm-6">
                                <h4 class="tit-chamada">Módulo 5 / Aula 6</h4>
                            </div>

                            <div class="col-sm-6 d-flex justify-content-end">
                                <a href="" class="btn btn-pointcom-outlined <!--concluido -->">
                                    Assistiu? <!-- Assisti <span class="material-icons-outlined">check_circle_outline</span> -->
                                </a>
                            </div>

                            <div class="col-sm-12">
                                <h1 class="titulo-aulas">Aula 06 - Luzes Artificiais do V-Ray</h1>
                                <h6 class="text-muted">
                                    Essa é a introdução ou pré alinhamento do curso.
                                </h6>

                                <div class="pointcom-reviews d-flex">
                                    <span class="chamada text-muted">Gostou dessa aula? </span>
                                    <span class="material-icons icon">star</span>
                                    <span class="material-icons icon">star</span>
                                    <span class="material-icons icon">star</span>
                                    <span class="material-icons icon">star</span>
                                    <span class="material-icons-outlined icon-no">star_outline</span>
                                </div>

                                <hr>
                            </div>

                            <div class="col-sm-12">
                                <!-- aqui vai o conteudo -->
                                <h4><b>Parte de texto</b></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>

                        </div>
                    </div>
                </div><!-- end:: card -->

                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title internas">VÍDEOS COMPLEMENTARES</h3>
                        <hr>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="pointcom-video">
                                    <iframe src="https://player.vimeo.com/video/49935360?color=e0a581&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                </div>
                                <p>Breve descrição do material complementar</p>
                            </div>
                            <div class="col-sm-4">
                                <div class="pointcom-video">
                                    <iframe src="https://player.vimeo.com/video/49935360?color=e0a581&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                </div>
                                <p>Breve descrição do material complementar</p>
                            </div>
                            <div class="col-sm-4">
                                <div class="pointcom-video">
                                    <iframe src="https://player.vimeo.com/video/49935360?color=e0a581&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                </div>
                                <p>Breve descrição do material complementar</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title internas">ARQUIVOS COMPLEMENTARES</h3>
                        <hr>
                    </div>

                    <div class="table-responsive card-body">
                        <table class="table">
                            <tbody>
                                <!-- LOOP:: START:: ROW TABLE -->
                                <tr>
                                    <td width="80">
                                        <a href="" class="pointcom-icone">
                                            <img src="./assets/img/icone.png" alt="">
                                            <span>pdf</span>
                                        </a>
                                    </td>

                                    <td class="text-muted align-middle">
                                        Arquivo do Mapa Mental da Aula
                                    </td>

                                    <td width="200" class="align-middle">
                                        <a href="" class="btn btn-pointcom-outlined">
                                            Baixar Arquivo
                                        </a>
                                    </td>
                                </tr>
                                <!-- LOOP:: END:: ROW TABLE -->


                                <!-- LOOP:: START:: ROW TABLE -->
                                <tr>
                                    <td width="80">
                                        <a href="" class="pointcom-icone">
                                            <img src="./assets/img/icone.png" alt="">
                                            <span>skp</span>
                                        </a>
                                    </td>

                                    <td class="text-muted align-middle">
                                        Arquivo de SKetchup
                                    </td>

                                    <td width="200" class="align-middle">
                                        <a href="" class="btn btn-pointcom-outlined">
                                            Baixar Arquivo
                                        </a>
                                    </td>
                                </tr>
                                <!-- LOOP:: END:: ROW TABLE -->


                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- end:: .col-sm-12 global -->
        </div><!-- end:: .row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="pointcom-box prev">
                    <p>Aula anterior</p>
                    <h3>Luzes do V-Ray</h3>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="pointcom-box next">
                    <p>Próxima aula</p>
                    <h3>Luzes Naturais</h3>
                </div>
            </div>
        </div>
    </div>
</section>



<?php include( 'footer.php' ); ?>