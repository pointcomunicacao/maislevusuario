<?php include( 'header.php' ); ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="titulo-secao">Bônus</h2>
            </div>

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="card">
                    <div class="box-bonus">
                        <img src="./assets/img/teste-logo-lev-conceito.jpg" alt="" class="img-fluid">
                        <span>Bônus #1</span>
                        <h2>Excepteur adipisicing irure esse cillum.</h2>
                        <p>Minim exercitation amet Lorem non incididunt exercitation ullamco do ad est.</p>
                        <a href="aula-sendo-apresentada.php" class="btn btn-padrao">Acessar Bônus</a>
                    </div>
                </div>
            </div>
            <!-- end:: loop -->

            <!-- start:: loop -->
            <div class="col-sm-4">
                <div class="card">
                    <div class="box-bonus">
                        <img src="./assets/img/teste-logo-lev-conceito.jpg" alt="" class="img-fluid">
                        <span>Bônus #1</span>
                        <h2>Excepteur adipisicing</h2>
                        <p>Minim exercitation amet Lorem non incididunt exercitation ullamco do ad est.</p>
                        <a href="aula-sendo-apresentada.php" class="btn btn-padrao">Acessar Bônus</a>
                    </div>
                </div>
            </div>
            <!-- end:: loop -->

        </div>
    </div>
</section>

<?php include( 'footer.php' );